import Layout from '../components/Layout'
import Link from 'next/link'

function getPosts() {
    return [
        { id: 'hello-nextjs', title:'Hello Next.js' },
        { id: 'learn-nextjs', title:'Learn Next.js' },
        { id: 'deploy-nextjs', title:'Deploy apps' }
    ]
}

const PostLink = ({ post }) => (
    <React.Fragment>
        <Link as={`/page/${post.id}`} href={`/page/${post.title}`}>
            <a>{post.title}</a>
        </Link>
        <style jsx>{`
        a {
            text-decoration: none;
            color: blue;
            font-family: "Arial";
          }

          a:hover {
            opacity: 0.6;
          }
        `}
        </style>
    </React.Fragment>
)

export default () => (
    <Layout>
        <h1>My Blog</h1>
        <ul>
        {getPosts().map((post) => (
            <li key={post.id}>
                <PostLink post={post}/>
            </li>
        ))}
        </ul>
        <style jsx>{`
           h1, a {
            font-family: "Arial";
          }

          ul {
            padding: 0;
          }

          li {
            list-style: none;
            margin: 5px 0;
          }
        `}
        </style>
    </Layout>
)

