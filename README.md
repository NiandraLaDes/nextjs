# Project Title

NextJS - Getting Started Tutorial

# Description

A 'Getting started' tutorial for NextJS -  A lightweight framework for static and server-rendered applications.

## Built With

* [React.js](https://reactjs.org/) - A JavaScript library for building user interfaces.
* [Next.js](https://nextjs.org/) - A lightweight framework for static and server-rendered applications.

## Authors

* **Bart van Enter** - *Initial work* - [Bitbucket](https://bitbucket.org/NiandraLaDes) and [Enter Webdevelopment](https://enter-webdevelopment.nl/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details