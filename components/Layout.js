import Header from './Header'

const layoutStyle = {
    margin: 20,
    padding: 20,
    backgroundColor: '#C1DDD3'
}

const Layout = (props) => (
    <div style={layoutStyle}>
        <Header />
        {props.children}
    </div>
)

export default Layout
