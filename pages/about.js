import Link from 'next/link'
import Layout from '../components/Layout'

const About = () => (
    <Layout>
        <h1>About</h1>
        <p>This is the about page</p>
        Click{' '}
        <Link href="/" replace>
            <a>here</a>
        </Link>{' '}
        to read more
    </Layout>
)

export default About;
